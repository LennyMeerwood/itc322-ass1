import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * The Menu class is responsible for the view of the program.
 * @author Leonard Meerwood
 *
 */
public class Menu {

	//Scanner for input and Controller for control
	private Scanner sc;
	private Controller c;
	
	/**
	 * Initiates the default menu.
	 */
	public Menu() {
		sc = new Scanner(System.in);
		c = new Controller();
	}
	
	/**
	 * Intiates the default menu with a file preloaded.
	 * @param fileName
	 * 	File to be preloaded.s
	 */
	public Menu(String fileName) {
		
		this();
		
		//Try load the file.
		try {
			c.loadFile(fileName);
			System.out.println("FILE SUCCESSFULLY LOADED");
		
		//Handle the various exceptions thrown.
		} catch (FileNotFoundException e) {
			this.displayError("FILE PASSED VIA COMMAND LINE DOES NOT EXIST");
			
		} catch (IOException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (NullPointerException e) {
			this.displayError("FILE PASSED VIA COMMAND LINE WAS INVALID. CHECK STRUCTURE.");
			
		}
	}
	
	/**
	 * Responsible for running the menu. Will display prompts and act according to the
	 * users reply.
	 */
	public void runMenu() {
		
		boolean running = true;
		
		//working loop.
		while (running) {
			System.out.println(mainMenuText);
			
			//The users input is wrapped inside of a try to allow us to handle
			//them putting in values besides numbers without the program crashing.
			try {
				int menuOption = sc.nextInt();
				
				//different actions depending on user input.
				switch (menuOption) {
				case 1:
					this.loadDialogue();
					break;
				case 2:
					this.sillInformation();
					break;
				case 3:
					this.answerInformation();
					break;
				case 4:
					this.exitMessage();
					running = false;
					break;
				default:
					this.invalidInputMessage();
				}
			} catch (Exception e) {
				//This catch is what handles non numeric inputs.
				this.displayError(e.getLocalizedMessage());
			}	
		}
	}
	
	/**
	 * Prompts the user for the file location that they wish to load. It then
	 * passes that to the {@link Controller} who tries to load it. If loading fails
	 * then an exception is thrown from the {@link Controller} and an appropriate
	 * error message is displayed.
	 */
	private void loadDialogue() {
		
		//Prompt for input
		System.out.println(loadFileText);
		//Retrieve input
		String file = sc.next();
		
		//Try load the file.
		try {
			c.loadFile(file);
		
		//Handle the various exceptions thrown.
		} catch (FileNotFoundException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (IOException e) {
			this.displayError(e.getLocalizedMessage());
			
		} catch (NullPointerException e) {
			this.displayError("File was not in the correct format. Please check file and try again.");
			
		}
		
	}
	
	/**
	 * Simply display a formatted string displaying information about the two sills that have
	 * been loaded.
	 */
	private void sillInformation() {
		try {
			String output = String.format(
					displaySillInformation,
					c.retrieveLengthA(),
					c.retrieveContentsA(),
					c.retrieveLengthB(),
					c.retrieveContentsB());
			System.out.println(output);
		} catch (IllegalStateException e) {
			this.displayError(e.getLocalizedMessage());
		}		
	}
	
	/**
	 * Simply display a formatted string displaying information abuot the answer
	 */
	private void answerInformation() {
		try {
			String output = String.format(
					displaySillAddition,
					c.retrieveContentsA(),
					c.retrieveContentsB(),
					c.retrieveContentsAnswer(),
					c.retrieveLengthAnswer());
			System.out.println(output);
		} catch (IllegalStateException e) {
			this.displayError(e.getLocalizedMessage());
		}		
	}
	
	/**
	 * A simple function to display a formatted error message.
	 * @param error
	 * 	The error to be included in the message.
	 */
	private void displayError(String error) {
		String errorMessage = String.format(errorString, error);
		System.out.println(errorMessage);
	}

	/**
	 * Prints an exit message.
	 */
	private void exitMessage() {
		System.out.println(exitProgram);
	}
	
	/**
	 * Print a message stating invalid input.
	 */
	private void invalidInputMessage() {
		System.out.println(invalidInput);
	}
	
	
	//These are String constants used for longer messages.
	private final String mainMenuText = 
			"=========================================================\n"
		  + " Press 1 to read number from file\n"
		  + " Press 2 to display integers stored in SILL\n"
		  + " Press 3 to display addition result\n"
		  + " Press 4 to exit the program\n"
		  + "=========================================================";
	
	private final String loadFileText =
			"Please enter the file name: ";
	
	private final String displaySillInformation = 
			"Sill 1 has %d node(s). Its value is\n%s\n"
			+ "Sill 2 has %d node(s). Its value is\n%s\n";
	
	private final String displaySillAddition = 
			"%s\n+\n%s\n=========================\n%s\n"
			+ "It requires %d node(s) to store the answer.\n";
	
	private final String exitProgram = 
			"Thanks for using the program!\n";
	
	private final String invalidInput =
			"That is not a menu option. Please choose from option 1-4\n";
	
	private final String errorString = 
			"Error! %s\n";
}
