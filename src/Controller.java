import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * The Controller class provides an interface between {@link Menu} and
 * {@link SingleIntegerLinkedList}.
 * @author Leonard Meerwood
 *
 */
public class Controller {
	
	//Three lists, two for the inputs and one for the answer.
	private SingleIntegerLinkedList listA, listB, listAnswer;
	
	//A flag to see if the list is loaded or not
	private boolean listLoaded;
	
	/**
	 * Contrustor sets the listLoaded flag to false
	 */
	public Controller() {
		this.listLoaded = false;
	}
	
	/**
	 * Loads the values in the fileName in to listA and listB. 
	 * Then adds the two together and stores the answer.
	 * @param fileName
	 *  The file to be loaded.
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void loadFile(String fileName) throws FileNotFoundException, IOException {
		
		//If the filename doesn't end in .txt, it is added.
		if (!fileName.endsWith(".txt")) {
			fileName = fileName + ".txt";
		}
		
		//A try-with-resources block that tries to load the values.
		try (BufferedReader br = 
				new BufferedReader(new FileReader(fileName)))  {
			listA = new SingleIntegerLinkedList(br.readLine());
			listB = new SingleIntegerLinkedList(br.readLine());
			listAnswer = listA.add(listB);
			this.listLoaded = true;
		}
	}
	
	/** 
	 * Retrieves the value in listA.
	 * @return
	 * 	String containing the value of listA
	 * @throws IllegalStateException
	 */
	public String retrieveContentsA() throws IllegalStateException {
		this.checkFileLoaded();
		return listA.toString();
	}
	
	/**
	 * Retrieves the number of nodes used in listA
	 * @return
	 *  int holding the number of nodes used in listA
	 * @throws IllegalStateException
	 */
	public int retrieveLengthA() throws IllegalStateException {
		this.checkFileLoaded();
		return listA.getSize();
	}
	
	/** 
	 * Retrieves the value in listB.
	 * @return
	 * 	String containing the value of listB
	 * @throws IllegalStateException
	 */
	public String retrieveContentsB() throws IllegalStateException {
		this.checkFileLoaded();
		return listB.toString();
	}
	
	/**
	 * Retrieves the number of nodes used in listB
	 * @return
	 *  int holding the number of nodes used in listB
	 * @throws IllegalStateException
	 */
	public int retrieveLengthB() throws IllegalStateException {
		this.checkFileLoaded();
		return listB.getSize();
	}
	
	/** 
	 * Retrieves the value in listAnswer.
	 * @return
	 * 	String containing the value of listAnswer
	 * @throws IllegalStateException
	 */
	public String retrieveContentsAnswer() throws IllegalStateException {
		this.checkFileLoaded();
		return listAnswer.toString();
	}
	
	/**
	 * Retrieves the number of nodes used in listAnswer
	 * @return
	 *  int holding the number of nodes used in listAnswer
	 * @throws IllegalStateException
	 */
	public int retrieveLengthAnswer() throws IllegalStateException {
		this.checkFileLoaded();
		return listAnswer.getSize();
	}
	
	/**
	 * A quick check to see if the listLoaded flag is set. If not, throws 
	 * an exception.
	 * @throws IllegalStateException
	 */
	private void checkFileLoaded() throws IllegalStateException {
		if (!listLoaded) {
			throw new IllegalStateException("No file has been loaded.");
		}
	}
}
