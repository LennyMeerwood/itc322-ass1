
/**
 * A modified version of the Doubly Linked list from the textbook. 
 * @author Leonard Meerwood, Reoberto Tamassia, Michael T. Goodrich
 *
 */
public class SingleIntegerLinkedList {
	
	/**
	 * A modified version of the Node for doubly linked list from the textbook.
	 * Main difference is that this node can only take intergers instead of 
	 * generics.
	 * @author Leonard Meerwood, Reoberto Tamassia, Michael T. Goodrich
	 *
	 */
	private static class Node  {
		//variables
		private int value;
		private Node next;
		private Node previous;
		
		/**
		 * Default contstructor
		 * @param val
		 * 	Value to be stored.
		 * @param p
		 * 	Previous node
		 * @param n
		 * 	Next node
		 */
		public Node(int val, Node p, Node n) {
			value = val;
			next = n;
			previous = p;
		}
		
		/**
		 * Gets the value stored in the {@link Node}.
		 * @return
		 * 	Value in node
		 */
		public int getValue() {
			return value;
		}
		
		/**
		 * Gets the {@link Node} after the current one.
		 * @return
		 * 	next {@link Node}
		 */
		public Node getNext() {
			return next;
		}
		
		/**
		 * Changes the next pointer to a new {@link Node}
		 * @param n
		 * 	New {@link Node} to be pointed to
		 */
		public void setNext(Node n) {
			next = n;
		}
		
		/**
		 * Get the {@link Node} prior to the current one.
		 * @return
		 * 	The prior {@link Node}
		 */
		public Node getPrevious() {
			return previous;
		}
		
		/**
		 * Changes the previous pointer to a new {@link Node}
		 * @param p
		 * 	New {@link Node} to be pointed to.
		 */
		public void setPrevious(Node p) {
			previous = p;
		}
		
	}
	
	//Member variables.
	private Node header;
	private Node trailer;
	private int size = 0;
	private boolean negative = false;
	
	/**
	 * Checks if {@link SingleIntegerLinkedList} is empty.
	 * @return
	 * 	Empty status
	 */
	public boolean isEmpty() {
		return size == 0;
	}
	
	/**
	 * Checks if the {@link SingleIntegerLinkedList} is negative.
	 * @return
	 * 	Negative status
	 */
	public boolean isNegative() {
		if (size == 1 && header.getNext().getValue() == 0) {
			return false;
		}
		return negative;
	}
	
	/**
	 * Set the {@link SingleIntegerLinkedList} to negative
	 * @param neg
	 * 	Set to negative or not
	 */
	public void setNegative (boolean neg) {
		negative = neg;
	}
	
	/**
	 * Default Constructor.
	 */
	public SingleIntegerLinkedList() {
		header = new Node(-1, null, null);
		trailer = new Node(-1, header, null);
		header.setNext(trailer);
	}
	
	/**
	 * Return the value of the first node.
	 * @return
	 * 	Value of first node.
	 */
	public int first() {
		if (isEmpty()) return -1;
		return header.getNext().getValue();
	}
	
	/**
	 * Return the value of the last node.
	 * @return
	 * 	Value of last node
	 */
	public int last() {
		if (isEmpty()) return -1;
		return trailer.getPrevious().getValue();
	}
	
	/**
	 * Add a new {@link Node} to the start of the list
	 * @param val
	 * 	The value to store in the new {@link Node} at the start
	 */
	public void addFirst(int val) {
		addBetween(val, header, header.getNext());	
	}
	
	/**
	 * Add a new {@link Node} to the end of the list
	 * @param val
	 * 	The value to store in the new {@link Node} at the end
	 */
	public void addLast(int val) {
		addBetween(val, trailer.getPrevious(), trailer);
	}
	
	/**
	 * A function that inserts a node between two other nodes
	 * @param val
	 * 	The value to store in the node
	 * @param predecessor
	 * 	The node before the new node
	 * @param successor
	 * 	The node after the new node
	 */
	private void addBetween (int val, Node predecessor, Node successor) {
		Node newest = new Node(val, predecessor, successor);
		predecessor.setNext(newest);
		successor.setPrevious(newest);
		size++;
	}
	
	/**
	 * Constructs a {@link SingleIntegerLinkedList} with the numeric value of the 
	 * string passed in.
	 * @param input
	 * 	String value to be stored. Must be a whole number format.
	 */
	public SingleIntegerLinkedList(String input) {
		
		this();
		
		//Only initialize if string is greater than zero.
		if(input.length()!=0) {		
			//Regex check to make sure the input is valid. If not throw exception
			if(!input.matches("^-?[0-9,]+$")) {
				throw new IllegalArgumentException("Input must contain only number and commas, and may start with a dash");
			}
			
			//Zero's won't be added to the linked list until one non-zero digit has been added.
			//This prevents zero padding to the left.
			boolean removedPadding = false;
			
			//Iterate through characters, convert them to intergers and then add them to linked list
			for (int i = 0; i < input.length(); i++ ) {
				char c = input.charAt(i);
				
				//A check to see if the first character is a dash. If it is
				//then toggle the SILL to be negative.
				if(i == 0 && c == '-') {
					negative = true;
				} else {
					//A comparison to check if there are any leading zeros. If there is
					//ignore them. This removes any left hand padding. Otherwise, add 
					//the value to the end of the list.
					if (c == '0' && removedPadding) {
						addLast(Integer.parseInt(c + ""));
					} else if (c != '0') {
						addLast(Integer.parseInt(c + ""));
						removedPadding = true;
					}
				}
			}	
		}
	}
	
	/**
	 * Returns the numerical value of the {@link SingleIntegerLinkedList} as a {@link String}
	 * @return
	 * 	String containing value
	 */
	public String toString() {
		//Check if the list is empty. If it is then just return zero.
		if(isEmpty()) {
			return "0";
		}
		
		//String builder to store answer.
		StringBuilder sb = new StringBuilder();
		
		//add negative sign if negative.
		if(negative) {
			sb.append("-");
		}
		
		//Iterate through all the nodes and add them to the output.
		Node current = header.getNext();
		for (int i = 0; i < size; i++) {
			sb.append(current.getValue());
			current = current.getNext();
		}
		
		//return the output.
		return sb.toString();
	}
	
	/**
	 * Returns the size of the {@link SingleIntegerLinkedList}
	 * @return
	 * 	Size
	 */
	public int getSize() {
		return size;
	}
	
	/**
	 * Returns the value from {@link Node} n from the end
	 * @param number
	 * 	The node to return
	 * @return
	 * 	The value stored in the node
	 */
	public int getFromEnd(int number) {
		
		//we are returning the digit in the 
		//10^number location. If there is no
		//digit at that location then that 
		//must mean the value is zero.
		if (number >= size || isEmpty()) {
			return 0;
		}
		
		//Iterate backwards through the list
		//until we find the node we need.
		Node n = trailer.getPrevious();
		for (int i = 0; i < number; i++) {
			n = n.getPrevious();
		}
		
		//return the value in the node.
		return n.getValue();
	}
	
	/**
	 * Adds a {@link SingleIntegerLinkedList} to this
	 * @param that
	 * 	The {@link SingleIntegerLinkedList} to add
	 * @return
	 * 	A {@link SingleIntegerLinkedList} storing the answer.
	 */
	public SingleIntegerLinkedList add (SingleIntegerLinkedList that) {
		
		//Check if only one value is negative. If so, then subtract the two values
		//instead of add.
		if(this.isNegative() ^ that.isNegative()) {
			return this.subtract(that);
		}
		
		//Carryover is used to store any values to be carried to the next value
		int carryOver = 0;
		
		//A list to hold the answer.
		SingleIntegerLinkedList answer = new SingleIntegerLinkedList();
		
		//get the biggest size and use that for the iterator
		int size = (this.getSize() > that.getSize()) ? this.getSize() : that.getSize();
		
		//ints to store the values.
		int thisVal;
		int thatVal;
		int result;
		
		//Iterate of the list and add them together one at a time.
		//Adds them together from smallest to largest.
		for (int i = 0; i < size; i++) {
			thisVal = this.getFromEnd(i);
			thatVal = that.getFromEnd(i);

			//Add the two values and check if larger then 10.
			//If so remove 10 and set carryOver to 1. Otherwise
			//Carryover is 0
			result = thisVal + thatVal + carryOver;
			if (result > 9) {
				carryOver = 1;
				result -= 10;
			} else {
				carryOver = 0;
			}
			answer.addFirst(result);	
		}
		
		//If there is any carrover left over after all the nodes have been added then
		//add the carryover as a new node in the highest value.
		if (carryOver > 0) {
			answer.addFirst(carryOver);
		}
		
		//Valeus are only added together if both are positive or both are negative.
		//Therefore, setting the answer to the same polarity as this list will result
		//in the correct value.
		answer.setNegative(this.isNegative());
		return answer;	
	}
	
	
	/**
	 * Uses a method called 9's compliment to subtract two SingleIntergerLinkedList from each other
	 * @param that
	 * 	The list to subtract
	 * @return
	 * 	A new SingleIntergerLinkedList that holds the answer
	 */
	public SingleIntegerLinkedList subtract (SingleIntegerLinkedList that) {
		
		SingleIntegerLinkedList minuend, subtrahend;
		
		//The list with the greater magnitude must be the minuend
		if (this.hasGreaterMagnitude(that) != -1) {
			minuend = this;
			subtrahend = that.clone();
		} else {
			minuend = that;
			subtrahend = this.clone();
		}
		
		subtrahend.setNegative(false);
		
		//get the 9's compliment of the minuend
		SingleIntegerLinkedList ninesCompliment = new SingleIntegerLinkedList();
		
		for (int i = 0; i < minuend.getSize(); i++) {
			int compliment = 9 - minuend.getFromEnd(i);
			ninesCompliment.addFirst(compliment);
		}
		
		//Add the 9's complimennt of the minuend to the subtrahend
		SingleIntegerLinkedList result = ninesCompliment.add(subtrahend);
		
		//A list to store the answer.
		SingleIntegerLinkedList answer = new SingleIntegerLinkedList();
		
		//Get the 9's compliment of the result and store it in the answer list.
		for (int i = 0; i < result.getSize(); i++) {
			int compliment = 9 - result.getFromEnd(i);
			answer.addFirst(compliment);
		}
		
		//Change the negativity based on the minuend.
		answer.setNegative(minuend.isNegative());
		
		//Remove padding
		answer.removePadding();
		
		//Return answer.
		return answer;
	}
	
	/**
	 * Compares another SingleIntergerLinkedList to itself. Returns values
	 * depending on which list has a greater magnitude
	 * @param that
	 * 	The other SingleIntergerLinkedList to compare
	 * @return
	 * 	 1 if this list is greater, -1 if the other list is greater or 0 if they are the same
	 */
	public int hasGreaterMagnitude(SingleIntegerLinkedList that) {
		
		//Check which size is bigger. Bigger size == bigger magnitude.
		if (this.getSize() > that.getSize()) {
			return 1;
		} else if (this.getSize() < that.getSize()) {
			return -1;
		}
		
		//If we got this far both sizes must be the same.
		//We check each individual value to see which is bigger.
		//We start at the most significant place and work towards least.
		int size = this.getSize();
		int thisVal, thatVal;
		for(int i = size - 1; i > 0; i--) {
			thisVal = this.getFromEnd(i);
			thatVal = that.getFromEnd(i);
			if (thisVal > thatVal) {
				return 1;
			} else if (thisVal < thatVal) {
				return -1;
			}
		}
		return 0;
	}
	
	/**
	 * Removes the excess zero's on the left hand side
	 */
	public void removePadding() {
		while(header.getNext().getValue() == 0 && size > 0) {
			Node removee = header.getNext();
			header.setNext(removee.getNext());
			removee.getNext().setPrevious(header);
			size--;
		}
		
		if (size == 0) {
			this.setNegative(false);
		}
	}
	
	/**
	 * A simple function to clone an existing {@link SingleIntegerLinkedList}
	 */
	public SingleIntegerLinkedList clone() {
		SingleIntegerLinkedList clone = new SingleIntegerLinkedList();
		for(int i = 0; i < this.getSize(); i++) {
			clone.addFirst(this.getFromEnd(i));
		}
		
		return clone;
	}
}
